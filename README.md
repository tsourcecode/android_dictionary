Тестовое задание:

Необходимо разработать приложение, в котором пользователь может составить свой личный словарь незнакомых иностранных слов.
Пользователь вводит слово, слово добавляется в его персональный словарь и к слову подгружается перевод (например с http://api.yandex.ru/translate/)
Пользователю выводятся все добавленные слова с переводами в виде списка.
Так же пользователь может найти ранее добавленное слово в своем словаре используя функцию поиска по слову или по его переводу.

Минимальные требования для Android: Android 4.0 и выше. Использование сторонних библиотек не запрещается, при условии, что их использование уместно и оправданно.