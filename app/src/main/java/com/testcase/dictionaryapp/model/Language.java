package com.testcase.dictionaryapp.model;

public class Language {
    private String name;
    private String code;

    @Override
    public String toString() {
        return name+"("+code+")";
    }

    public Language(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
