package com.testcase.dictionaryapp.model;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;

public class YandexApiResult {
    private final static String YANDEX_API = "trnsl.1.1.20150310T122015Z.ac140f5b05fe1e49.d9a24f51c0a3a97aa52545bb9ba42d9aecc1f1cf";
    private final static String TRANSLATE_URL = "https://translate.yandex.net/api/v1.5/tr.json/translate";
    public final static int CODE_SUCCESS = 200;

/**
 {
     "code": 200,
     "lang": "en-ru",
     "text": [
         "время для разговора!"
     ]
 }

 */
    private int code;
    private String lang;
    private ArrayList<String> text;
    private String message;

    public static YandexApiResult newInstance(String text, Language srcLng, Language dstLng) throws JsonSyntaxException, IOException, NullPointerException{
        /*
            https://translate.yandex.net/api/v1.5/tr.json/translate ?
            key=<API-ключ>
             & text=<переводимый текст>
             & lang=<направление перевода>
             & [format=<формат текста>]
             & [options=<опции перевода>]
             & [callback=<имя callback-функции>]
         */
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("key", YANDEX_API));
        nameValuePairs.add(new BasicNameValuePair("text", text));
        nameValuePairs.add(new BasicNameValuePair("lang", srcLng.getCode()+"-"+dstLng.getCode()));
        HttpPost httpPost = new HttpPost(TRANSLATE_URL);

        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));

        BasicHttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, 5000);

        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

        HttpResponse httpResponse = httpClient.execute(httpPost);

        if (httpResponse != null){
            String json = EntityUtils.toString(httpResponse.getEntity());
            return new Gson().fromJson(json, YandexApiResult.class);
        }

        throw new NullPointerException("no execution response received");
    }

    public int getCode() {
        return code;
    }

    public String getLang() {
        return lang;
    }

    public String getResult() {
        return text != null && code == CODE_SUCCESS
                ?TextUtils.join("", text.toArray())
                :null;
    }

    public String getErrorMessage(){
        return message != null || code == CODE_SUCCESS ? message : "unknown error (code "+code+")";
    }
}
