package com.testcase.dictionaryapp.model;

import android.content.Context;

import com.testcase.dictionaryapp.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LanguageList extends ArrayList<Language> {
    private final List<String> keyList;
    private final List<String> nameList;

    public LanguageList(Context context){
        keyList = Arrays.asList(context.getResources().getStringArray(R.array.language_keys));
        nameList = Arrays.asList(context.getResources().getStringArray(R.array.language_names));

        if (keyList.size() != nameList.size()){
            throw new ArrayIndexOutOfBoundsException("keys and names arrays sizes do not match");
        }

        for (int i = 0; i < keyList.size(); i++) {
            add(new Language(keyList.get(i), nameList.get(i)));

        }
    }

    public Language findByCode(String code){
        int index = keyList.indexOf(code);
        return index != -1 ? get(index) : null;
    }

    public Language findByName(String name){
        int index = nameList.indexOf(name);
        return index != -1 ? get(index) : null;
    }

    public List<String> getNames() {
        return nameList;
    }

    public List<String> getCodes() {
        return keyList;
    }
}
