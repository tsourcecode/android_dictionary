package com.testcase.dictionaryapp;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;

import com.google.gson.JsonSyntaxException;
import com.testcase.dictionaryapp.database.CpContract;
import com.testcase.dictionaryapp.model.Language;
import com.testcase.dictionaryapp.model.LanguageList;
import com.testcase.dictionaryapp.model.YandexApiResult;

import java.io.IOException;

public class TranslateService extends IntentService{
    private LanguageList languageList;
    private static final int MAX_ATTEMPTS_COUNT = 10;

    public TranslateService() {
        super(TranslateService.class.getName());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        languageList = new LanguageList(getApplicationContext());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Cursor cursor = getContentResolver().query(
                CpContract.Dictionary.CONTENT_URI,
                CpContract.Dictionary.PROJECTION_ALL,
                CpContract.Dictionary.COLUMN_TRAN + " is null", null,
                CpContract.Dictionary._ID+" DESC limit 1");
        int failCount = 0;
        while (cursor != null && cursor.getCount() > 0){
            cursor.moveToFirst();
            Language srcLang = languageList.findByCode(cursor.getString(cursor.getColumnIndex(CpContract.Dictionary.COLUMN_LANG_SRC)));
            Language dstLang = languageList.findByCode(cursor.getString(cursor.getColumnIndex(CpContract.Dictionary.COLUMN_LANG_DST)));
            String id = cursor.getString(cursor.getColumnIndex(CpContract.Dictionary._ID));
            String word = cursor.getString(cursor.getColumnIndex(CpContract.Dictionary.COLUMN_WORD));
            cursor.close();

            ContentValues cv = new ContentValues();

            try {
                YandexApiResult result = YandexApiResult.newInstance(word, srcLang, dstLang);
                cv.put(CpContract.Dictionary.COLUMN_TRAN, result.getResult());
                cv.put(CpContract.Dictionary.COLUMN_ERROR, result.getErrorMessage());
            } catch (JsonSyntaxException | IOException | NullPointerException e) {
                cv.putNull(CpContract.Dictionary.COLUMN_TRAN);
                cv.put(CpContract.Dictionary.COLUMN_ERROR, e.toString());
                e.printStackTrace();

                if (!(e instanceof JsonSyntaxException)){
                    failCount++;
                }
            }


            Cursor modifyCheck = getContentResolver().query(
                    Uri.withAppendedPath(CpContract.Dictionary.CONTENT_URI, id),
                    new String[]{CpContract.Dictionary.COLUMN_WORD, CpContract.Dictionary.COLUMN_LANG_SRC, CpContract.Dictionary.COLUMN_LANG_DST},
                    null, null, null);
            boolean unchanged = false;

            if (modifyCheck != null && modifyCheck.getCount() == 1){
                modifyCheck.moveToFirst();

                unchanged = modifyCheck.getString(modifyCheck.getColumnIndex(CpContract.Dictionary.COLUMN_WORD)).equals(word) &&
                        modifyCheck.getString(modifyCheck.getColumnIndex(CpContract.Dictionary.COLUMN_LANG_SRC)).equals(srcLang.getCode()) &&
                        modifyCheck.getString(modifyCheck.getColumnIndex(CpContract.Dictionary.COLUMN_LANG_DST)).equals(dstLang.getCode());
            }

            if (unchanged){
                getContentResolver().update(Uri.withAppendedPath(CpContract.Dictionary.CONTENT_URI,id), cv, null, null);
            }

            if (failCount > MAX_ATTEMPTS_COUNT){
                return;
            }

            cursor = getContentResolver().query(
                    CpContract.Dictionary.CONTENT_URI,
                    CpContract.Dictionary.PROJECTION_ALL,
                    CpContract.Dictionary.COLUMN_TRAN + " is null", null,
                    CpContract.Dictionary._ID+" DESC limit 1");
        }
        }
}
