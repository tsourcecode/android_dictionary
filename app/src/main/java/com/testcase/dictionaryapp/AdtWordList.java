package com.testcase.dictionaryapp;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.testcase.dictionaryapp.database.CpContract;
import com.testcase.dictionaryapp.model.LanguageList;

public class AdtWordList extends CursorAdapter{
    private final LayoutInflater inflater;
    private final LanguageList languageList;

    public AdtWordList(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        languageList = new LanguageList(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View v = inflater.inflate(R.layout.adt_word_list, parent, false);
        v.setTag(new ViewHolder(v));
        return v;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        if (view.getTag() instanceof ViewHolder){
            ((ViewHolder)view.getTag()).bind(cursor);
        }
    }

    private class ViewHolder {
        TextView tvWord;
        TextView tvTranslation;
        TextView tvSrc;
        TextView tvDst;

        public ViewHolder(View v) {
            tvWord = (TextView) v.findViewById(R.id.adt_word_list_tv_word);
            tvTranslation = (TextView) v.findViewById(R.id.adt_word_list_tv_translation);
            tvSrc = (TextView) v.findViewById(R.id.adt_word_list_tv_tran_src);
            tvDst = (TextView) v.findViewById(R.id.adt_word_list_tv_tran_dst);
        }

        public void bind(Cursor cursor) {
            tvWord.setText(cursor.getString(cursor.getColumnIndex(CpContract.Dictionary.COLUMN_WORD)));
            tvTranslation.setText(cursor.getString(cursor.getColumnIndex(CpContract.Dictionary.COLUMN_TRAN)));

            tvSrc.setText(languageList.findByCode(cursor.getString(cursor.getColumnIndex(CpContract.Dictionary.COLUMN_LANG_SRC))).getName());
            tvDst.setText(languageList.findByCode(cursor.getString(cursor.getColumnIndex(CpContract.Dictionary.COLUMN_LANG_DST))).getName());
        }
    }
}
