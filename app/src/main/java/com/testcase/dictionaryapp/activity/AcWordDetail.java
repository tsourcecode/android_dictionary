package com.testcase.dictionaryapp.activity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.testcase.dictionaryapp.R;
import com.testcase.dictionaryapp.TranslateService;
import com.testcase.dictionaryapp.database.CpContract;
import com.testcase.dictionaryapp.model.Language;
import com.testcase.dictionaryapp.model.LanguageList;

import java.util.Calendar;

public class AcWordDetail extends ActionBarActivity implements View.OnClickListener, TextWatcher, Runnable, AdapterView.OnItemSelectedListener{
    public static final String EXTRA_WORD_ID = "word:id";
    private Spinner spSrc;
    private Spinner spDst;
    private LanguageList languageList;
    private EditText etInput;
    private Toolbar toolbar;
    private ImageButton btnSwitch;
    private Handler handler = new Handler();
    private TextView tvOutput;
    private String currentWord = "";
    private Language currentLngSrc;
    private Language currentLngDst;
    private long lastRequestTime;
    private ContentObserver contentObserver = new ContentObserver(handler) {
        @Override
        public void onChange(boolean selfChange, Uri uri) {
            super.onChange(selfChange, uri);
            if (!selfChange){
                Cursor cursor = getContentResolver().query(
                        Uri.withAppendedPath(CpContract.Dictionary.CONTENT_URI, getWordId()),
                        CpContract.Dictionary.PROJECTION_ALL, null, null, null);
                cursor.moveToFirst();

                String error = cursor.getString(cursor.getColumnIndex(CpContract.Dictionary.COLUMN_ERROR));
                if (error == null){
                    tvOutput.setText(cursor.getString(cursor.getColumnIndex(CpContract.Dictionary.COLUMN_TRAN)));
                }else{
                    tvOutput.setText(getString(R.string.error_translate) + "\n" + error);
                }
                cursor.close();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_word_detail);
        toolbar = (Toolbar) findViewById(R.id.ac_word_detail_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        languageList = new LanguageList(this);

        spSrc = (Spinner) findViewById(R.id.ac_word_detail_sp_from);
        spDst = (Spinner) findViewById(R.id.ac_word_detail_sp_to);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, languageList.getNames());
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spSrc.setAdapter(spinnerArrayAdapter);
        spDst.setAdapter(spinnerArrayAdapter);

        spSrc.setSelection(1);

        etInput = (EditText) findViewById(R.id.ac_word_detail_et_input);
        btnSwitch = (ImageButton) findViewById(R.id.ac_word_detail_btn_switch);
        tvOutput = (TextView) findViewById(R.id.ac_word_detail_tv_output);

        btnSwitch.setOnClickListener(this);

        if (getIntent().hasExtra(EXTRA_WORD_ID)){
            Cursor cursor = getContentResolver().query(
                    Uri.withAppendedPath(CpContract.Dictionary.CONTENT_URI, getIntent().getStringExtra(EXTRA_WORD_ID)),
                    CpContract.Dictionary.PROJECTION_ALL, null, null, null);
            if (cursor != null && cursor.getCount() > 0){
                cursor.moveToFirst();
                currentWord = cursor.getString(cursor.getColumnIndex(CpContract.Dictionary.COLUMN_WORD));

                spSrc.setSelection(languageList.getCodes().indexOf(cursor.getString(cursor.getColumnIndex(CpContract.Dictionary.COLUMN_LANG_SRC))));
                spDst.setSelection(languageList.getCodes().indexOf(cursor.getString(cursor.getColumnIndex(CpContract.Dictionary.COLUMN_LANG_DST))));
                tvOutput.setText(cursor.getString(cursor.getColumnIndex(CpContract.Dictionary.COLUMN_TRAN)));
                etInput.setText(currentWord);
            }else{
                Toast.makeText(this, R.string.error_not_found, Toast.LENGTH_SHORT).show();
                finish();
            }

            if (cursor != null){
                cursor.close();
            }
        }

        currentLngSrc = languageList.findByName(spSrc.getSelectedItem().toString());
        currentLngDst = languageList.findByName(spDst.getSelectedItem().toString());

        etInput.addTextChangedListener(this);
        if (etInput.getText() != null){
            etInput.setSelection(etInput.getText().toString().length());
        }

        spSrc.setOnItemSelectedListener(this);
        spDst.setOnItemSelectedListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                if (!TextUtils.isEmpty(etInput.getText())){
                    save();
                }

                Intent intent = new Intent();
                intent.setClass(this, AcMain.class);
                startActivity(intent);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private Uri save() {
        ContentValues cp = new ContentValues();
        cp.put(CpContract.Dictionary.COLUMN_WORD, etInput.getText().toString());
        Language srcLang = languageList.findByName(spSrc.getSelectedItem().toString());
        Language dstLang = languageList.findByName(spDst.getSelectedItem().toString());
        cp.put(CpContract.Dictionary.COLUMN_LANG_SRC, srcLang.getCode());
        cp.put(CpContract.Dictionary.COLUMN_LANG_DST, dstLang.getCode());

        if (getIntent().hasExtra(EXTRA_WORD_ID)){
            Uri uri = Uri.withAppendedPath(CpContract.Dictionary.CONTENT_URI, getIntent().getStringExtra(EXTRA_WORD_ID));
            getContentResolver().update(uri, cp, null, null);
            return uri;
        }else{
            return getContentResolver().insert(CpContract.Dictionary.CONTENT_URI, cp);
        }
    }

    @Override
    public void onClick(View v) {
        int pos = spSrc.getSelectedItemPosition();
        spSrc.setSelection(spDst.getSelectedItemPosition(), true);
        spDst.setSelection(pos, true);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (Calendar.getInstance().getTimeInMillis() - lastRequestTime > 1000){
            lastRequestTime = Calendar.getInstance().getTimeInMillis();
            handler.postDelayed(this, 1000);
        }

        if (TextUtils.isEmpty(s)){
            tvOutput.setText("");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        getContentResolver().unregisterContentObserver(contentObserver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getContentResolver().registerContentObserver(CpContract.Dictionary.CONTENT_URI, true, contentObserver);
    }

    private void sendTranslateRequest() {
        ContentValues cv = new ContentValues();
        currentLngSrc = languageList.findByName(spSrc.getSelectedItem().toString());
        currentLngDst = languageList.findByName(spDst.getSelectedItem().toString());
        cv.put(CpContract.Dictionary.COLUMN_LANG_SRC, currentLngSrc.getCode());
        cv.put(CpContract.Dictionary.COLUMN_LANG_DST, currentLngDst.getCode());
        cv.putNull(CpContract.Dictionary.COLUMN_TRAN);
        cv.putNull(CpContract.Dictionary.COLUMN_ERROR);
        cv.put(CpContract.Dictionary.COLUMN_WORD, etInput.getText().toString());
        getContentResolver().update(Uri.withAppendedPath(CpContract.Dictionary.CONTENT_URI, getWordId()), cv, null, null);
        Intent intent = new Intent();
        intent.setClass(this, TranslateService.class);
        startService(intent);
    }

    @Override
    public void run() {
        if (
                etInput != null && !TextUtils.isEmpty(etInput.getText()) && tvOutput != null &&
                !etInput.getText().toString().trim().toLowerCase().equals(currentWord)
        ){
            currentWord = etInput.getText().toString().trim().toLowerCase();
            sendTranslateRequest();
        }
    }

    private String getWordId(){
        if (!getIntent().hasExtra(EXTRA_WORD_ID)){
            String lastPathSegment = save().getLastPathSegment();
            getIntent().putExtra(EXTRA_WORD_ID, lastPathSegment);
            return lastPathSegment;
        }else{
            return getIntent().getStringExtra(EXTRA_WORD_ID);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (
                !TextUtils.isEmpty(etInput.getText()) &&
                (
                    !currentLngSrc.getName().equals(spSrc.getSelectedItem().toString())
                            ||
                    !currentLngDst.getName().equals(spDst.getSelectedItem().toString())
                )
        ){
            sendTranslateRequest();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
