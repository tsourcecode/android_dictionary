package com.testcase.dictionaryapp.activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.testcase.dictionaryapp.AdtWordList;
import com.testcase.dictionaryapp.R;
import com.testcase.dictionaryapp.database.CpContract;


public class AcMain extends ActionBarActivity implements AdapterView.OnItemClickListener, View.OnClickListener, TextWatcher, Runnable {
    private Toolbar toolbar;
    private EditText etSearch;
    private AdtWordList adapter;
    private Handler handler = new Handler();
    private TextView tvEmpty;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private ListView lvWordList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_main);

        toolbar = (Toolbar)findViewById(R.id.ac_main_toolbar);
        etSearch = (EditText) findViewById(R.id.ac_main_et_search);
        tvEmpty = (TextView) findViewById(R.id.ac_main_tv_empty_list);
        lvWordList = (ListView)findViewById(R.id.ac_main_listview);
        findViewById(R.id.ac_main_fab).setOnClickListener(this);


        setSupportActionBar(toolbar);

        adapter = new AdtWordList(this, getContentResolver().query(CpContract.Dictionary.CONTENT_URI, CpContract.Dictionary.PROJECTION_ALL, null, null, CpContract.Dictionary._ID + " DESC"), true);
        lvWordList.setAdapter(adapter);

        if (adapter.getCount() > 0){
            tvEmpty.setVisibility(View.GONE);
        }

        etSearch.addTextChangedListener(this);
        lvWordList.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent();
        intent.setClass(AcMain.this, AcWordDetail.class);
        intent.putExtra(AcWordDetail.EXTRA_WORD_ID, String.valueOf(id));
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        intent.setClass(AcMain.this, AcWordDetail.class);
        startActivity(intent);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        handler.postDelayed(this, 200);
    }

    @Override
    public void run() {
        if (etSearch != null){
            String filter = etSearch.getText() != null ?" like '%"+etSearch.getText().toString()+"%'":null;
            Cursor cursor = getContentResolver().query(
                    CpContract.Dictionary.CONTENT_URI,
                    CpContract.Dictionary.PROJECTION_ALL,
                    filter == null
                            ?null
                            :CpContract.Dictionary.COLUMN_WORD + filter+ " or "+ CpContract.Dictionary.COLUMN_TRAN+filter,
                    null, CpContract.Dictionary._ID + " DESC");
            adapter.changeCursor(cursor);
        }
    }
}
