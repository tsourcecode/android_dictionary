package com.testcase.dictionaryapp.database;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public class CpContract {
    public final static String AUTHORITY = "com.testcase.dictionaryapp.provider.default";
    /**
     * The content URI for the top-level authority.
     */
    public final static Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    /**
     * Constants for the Dictionary table.
     */
    public static final class Dictionary implements BaseColumns {
        /** table name */
        public static final String NAME = "dictionary";
        /**
         * The content URI for this table.
         */
        public static final Uri CONTENT_URI =  Uri.withAppendedPath(CpContract.CONTENT_URI, NAME);


        /** source language field */
        public static final String COLUMN_LANG_SRC = "lng_src";

        /** destination language field */
        public static final String COLUMN_LANG_DST = "lng_dst";

        /** source word field */
        public static final String COLUMN_WORD = "word";

        /** translated word field */
        public static final String COLUMN_TRAN = "tran";

        /** translation error field */
        public static final String COLUMN_ERROR = "error";

        /**
         * The mime type of a directory of dictionary.
         */
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/com.testcase.dictionaryapp.dictionary";

        /**
         * The mime type of a single word.
         */
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/com.testcase.dictionaryapp.dictionary";

        /**
         * A projection of all columns in the dictionary table.
         */
        public static final String[] PROJECTION_ALL = {_ID, COLUMN_WORD, COLUMN_LANG_SRC, COLUMN_LANG_DST, COLUMN_TRAN, COLUMN_ERROR};

        /**
         * The default sort order for queries containing NAME fields.
         */
        public static final String SORT_ORDER_DEFAULT = COLUMN_WORD + " ASC";

    }
}
