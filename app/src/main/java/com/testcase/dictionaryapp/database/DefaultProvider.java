package com.testcase.dictionaryapp.database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

public class DefaultProvider extends ContentProvider{

    // helper constants for use with the UriMatcher
    private static final int WORD_LIST = 1;
    private static final int WORD_ID = 2;

    private static final UriMatcher URI_MATCHER;

    private DbOpenHelper dbHelper = null;

    // prepare the UriMatcher
    static {
        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        URI_MATCHER.addURI(CpContract.AUTHORITY, CpContract.Dictionary.NAME, WORD_LIST);
        URI_MATCHER.addURI(CpContract.AUTHORITY, CpContract.Dictionary.NAME+"/#", WORD_ID);
    }

    @Override
    public boolean onCreate() {
        dbHelper = new DbOpenHelper(getContext());
        return true;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int delCount;
        switch (URI_MATCHER.match(uri)) {
            case WORD_LIST:
                delCount = db.delete(CpContract.Dictionary.NAME, selection, selectionArgs);
                break;
            case WORD_ID:
                String idStr = uri.getLastPathSegment();
                String where = CpContract.Dictionary._ID + " = " + idStr;
                if (!TextUtils.isEmpty(selection)) {
                    where += " AND " + selection;
                }
                delCount = db.delete(CpContract.Dictionary.NAME, where, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        // notify all listeners of changes:
        if (delCount > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return delCount;
    }

    @Override
    public String getType(Uri uri) {
        switch (URI_MATCHER.match(uri)) {
            case WORD_LIST:
                return CpContract.Dictionary.CONTENT_TYPE;
            case WORD_ID:
                return CpContract.Dictionary.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values){
        if (URI_MATCHER.match(uri) != WORD_LIST) {
            throw new IllegalArgumentException(
                    "Unsupported URI for insertion: " + uri);
        }
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long id = db.insert(CpContract.Dictionary.NAME, null, values);
        return getUriForId(id, uri);
    }

    private Uri getUriForId(long id, Uri uri) {
        if (id > 0) {
            Uri itemUri = ContentUris.withAppendedId(uri, id);
            // notify all listeners of changes and return itemUri:
            getContext().
                    getContentResolver().
                    notifyChange(itemUri, null);
            return itemUri;
        }

        return null;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        switch (URI_MATCHER.match(uri)) {
            case WORD_LIST:
                builder.setTables(CpContract.Dictionary.NAME);
                if (TextUtils.isEmpty(sortOrder)) {
                    sortOrder = CpContract.Dictionary.SORT_ORDER_DEFAULT;
                }
                break;
            case WORD_ID:
                builder.setTables(CpContract.Dictionary.NAME);
                // limit query to one row at most:
                builder.appendWhere(CpContract.Dictionary._ID + " = "
                        + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        Cursor cursor = builder.query(db, projection, selection, selectionArgs,
                null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int updateCount;
        switch (URI_MATCHER.match(uri)) {
            case WORD_LIST:
                updateCount = db.update(CpContract.Dictionary.NAME, values, selection,
                        selectionArgs);
                break;
            case WORD_ID:
                String idStr = uri.getLastPathSegment();
                String where = CpContract.Dictionary._ID + " = " + idStr;
                if (!TextUtils.isEmpty(selection)) {
                    where += " AND " + selection;
                }
                updateCount = db.update(CpContract.Dictionary.NAME, values, where,
                        selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        // notify all listeners of changes:
        if (updateCount > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return updateCount;
    }
}
