package com.testcase.dictionaryapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbOpenHelper extends SQLiteOpenHelper {
    private static final String NAME = "dictionaryDB";
    private static final int VERSION = 1;

    public DbOpenHelper(Context context) {
        super(context, NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE "+ CpContract.Dictionary.NAME+" (" +
                CpContract.Dictionary._ID+" INTEGER  PRIMARY KEY AUTOINCREMENT, \n" +
                CpContract.Dictionary.COLUMN_WORD +"     TEXT,\n" +
                CpContract.Dictionary.COLUMN_LANG_SRC +"     TEXT,\n" +
                CpContract.Dictionary.COLUMN_LANG_DST +"     TEXT,\n" +
                CpContract.Dictionary.COLUMN_TRAN +"     TEXT,\n" +
                CpContract.Dictionary.COLUMN_ERROR +"     TEXT\n" +
                ")"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // update not expected
    }
}
